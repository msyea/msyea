import 'prismjs'
import 'prismjs/components/prism-json.min.js'
import 'prismjs/components/prism-yaml.min.js'
import 'prismjs/components/prism-bash.min.js'

import 'bootstrap/dist/js/bootstrap.bundle.min.js'
import mermaid from 'mermaid'

if (document.querySelectorAll('.mermaid').length) {
  mermaid.initialize({ startOnLoad: true })
}
