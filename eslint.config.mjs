import globals from 'globals'
import pluginJs from '@eslint/js'
import stylistic from '@stylistic/eslint-plugin'

export default [
  {
    languageOptions: {
      globals: {
        document: 'readonly', // work around
        ...globals.node,
        ...globals.jest,
      },
    },
  },
  pluginJs.configs.recommended,
  stylistic.configs['recommended-flat'],
  {
    ignores: [
      'dist/*',
      '_site/*',
    ],
  },
]
