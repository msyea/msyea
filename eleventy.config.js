const path = require('path')

const sass = require('sass')

const { formatISO, format } = require('date-fns')

const markdownIt = require('markdown-it')
const markdownItAttrs = require('markdown-it-attrs')

module.exports = async function (eleventyConfig) {
  const { default: eleventyPluginGitLab } = await import('@msyea/eleventy-plugin-gitlab')
  eleventyConfig.addPlugin(eleventyPluginGitLab, {
    projectId: 48014208,
  })
  eleventyConfig.addTemplateFormats('scss')

  // Creates the extension for use
  eleventyConfig.addExtension('scss', {
    outputFileExtension: 'css', // optional, default: "html"

    // `compile` is called once per .scss file in the input directory
    compile: function (inputContent, inputPath) {
      let parsed = path.parse(inputPath)
      if (parsed.name.startsWith('_')) {
        return
      }

      let result = sass.compileString(inputContent, {
        loadPaths: [
          'node_modules',
          parsed.dir || '.',
          this.config.dir.includes,
        ],
      })
      this.addDependencies(inputPath, result.loadedUrls)

      return () => {
        return result.css
      }
    },
  })
  eleventyConfig.addPassthroughCopy({ 'dist/js': 'js' })
  eleventyConfig.addPassthroughCopy({
    'node_modules/bootstrap-icons/font/fonts': 'assets/css/fonts',
  })
  eleventyConfig.addPassthroughCopy({
    'assets/img': 'assets/img',
  })
  eleventyConfig.addPassthroughCopy('404.html')
  eleventyConfig.addPassthroughCopy('robots.txt')
  eleventyConfig.addLayoutAlias('default', 'layouts/default.njk')
  eleventyConfig.addLayoutAlias('naked', 'layouts/naked.njk')

  eleventyConfig.setFrontMatterParsingOptions({
    excerpt: true,
    // Optional, default is "---"
    excerpt_separator: '<!-- more -->',
  })

  const sortAndEnhance = (items) => {
    return items.sort((a, b) => {
      return b.date - a.date // Sort by date descending
    })
  }

  eleventyConfig.addCollection('allByDate', function (collectionApi) {
    const items = collectionApi.getAll()
    return sortAndEnhance(items)
  })

  eleventyConfig.addCollection('posts', function (collectionApi) {
    const items = collectionApi.getFilteredByGlob('./_posts/*.md')
    return sortAndEnhance(items)
  })

  eleventyConfig.addCollection('firstPost', function (collectionApi) {
    const items = collectionApi.getFilteredByGlob('./_posts/*.md')
    return sortAndEnhance(items).slice(0, 1)[0]
  })

  eleventyConfig.addCollection('remainingPosts', function (collectionApi) {
    const items = collectionApi.getFilteredByGlob('./_posts/*.md')
    return sortAndEnhance(items).slice(1)
  })

  eleventyConfig.addCollection('links', function (collectionApi) {
    const items = collectionApi.getFilteredByGlob('./_links/*.md')
    return sortAndEnhance(items)
  })

  eleventyConfig.addCollection('tagList', function (collectionApi) {
    let tagSet = new Set()
    collectionApi.getAll().forEach((item) => {
      if ('tags' in item.data) {
        let tags = item.data.tags
        tags = tags.filter(tag => tag !== 'all') // Exclude "all" from tags
        for (const tag of tags) {
          tagSet.add(tag)
        }
      }
    })
    return [...tagSet]
  })

  const md = markdownIt({
    html: true,
    linkify: true,
    typographer: true,
    code: true,
    highlight: function (str, lang) {
      if (lang === 'mermaid') {
        return '<pre><code class="mermaid">' + md.utils.escapeHtml(str) + '</code></pre>'
      }
      return ''
    },
  }).use(markdownItAttrs)
  md.renderer.rules.table_open = function () {
    return '<table class="table table-bordered">'
  }
  md.renderer.rules.blockquote_open = function () {
    return '<blockquote class="blockquote">\n'
  }

  eleventyConfig.setLibrary('md', md)
  eleventyConfig.addFilter('md', function (content = '') {
    return md.render(content) // Convert Markdown to HTML
  })

  eleventyConfig.addFilter('date_to_long_string', function (date) {
    return format(new Date(date), 'd MMMM yyyy')
  })

  eleventyConfig.addFilter('date_to_xmlschema', function (date) {
    return formatISO(new Date(date))
  })

  return {
    globalData: {
      title: 'Type II Fun',
      description: 'Type II Fun. The personal blog and website of Simon Mayes. Climber and Software Engineer.',
      baseurl: '/',
      site_url: process.env.SITE_URL || 'http://localhost:8080',
      my_categories: ['climbing', 'technology'],
      site_name: 'Type II Fun',
    },
  }
}
